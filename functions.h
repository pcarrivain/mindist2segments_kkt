/*!
  \file functions.h distributed under MIT licence.
  \brief Header file for functions used in the calculation of minimal distance between segment [P0,P0+L0*T0] and segment [P1,P1+L1*T1].
  \author Pascal Carrivain
  \date Monday 09 March 2020
*/
#ifndef UTILITAIRES2_H
#define UTILITAIRES2_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <assert.h>
#include <vector>
#include <cfloat>

/*!
  \struct vec3
  Definition of a vector struct x,y,z.
 */
struct vec3{
  double x;/*!< x coordinate */
  double y;/*!< y coordinate */
  double z;/*!< z coordinate */
  vec3(){/*! default constructor, 0 to each of the three coordinates*/
    x=0.;
    y=0.;
    z=0.;
  };
  vec3(double x1,double y1,double z1){/*! constructor, x=x1, y=y1 and z=z1*/
    x=x1;
    y=y1;
    z=z1;
  }
};

/*! return the dot-product of u with v.
 */
double dot_prod(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return the dot-product of u with itself.
 */
double dot_prod2(vec3 u/**< [in] struct vec3. */);

/*! return a*u where a is a scalar.
 */
vec3 mult3(double a/**< [in] double. */,vec3 u/**< [in] struct vec3. */);

/*! return u+v where u and v are vec3.
 */
vec3 add3(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return u-v where u and v are vec3.
 */
vec3 sub3(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return a vec3 u normalized to 1.
 */
vec3 normalize3(vec3 u/**< [in] struct vec3. */);

/*! @brief This function returns the two points (each on one segment) such that the distance is minimal.
  This function compares the result to an "exact enumeration" too (if test_result is true).
  The number of points (along each segment) is given by the integer variable enumeration.
  The precision of the "enumeration" increases with the integer variable enumeration.
  The function uses OpenMP "section" functionality to handle the different cases.
  @param A0 start of the first bond
  @param B0 end of the first bond
  @param A1 start of the second bond
  @param B1 end of the second bond
  @param test_result does the function test the result with an "exact enumeration" ?
  @param enumeration number of points to consider in the "exact enumeration"
*/
std::vector<vec3> minDist2segments_KKT(vec3 A0,vec3 B0,vec3 A1,vec3 B1,bool test_result,int enumeration);

/*! @brief This function returns the two points (each on one line) such that the distance is minimal.
  @param A0 start of the first bond
  @param B0 end of the first bond
  @param A1 start of the second bond
  @param B1 end of the second bond
*/
std::vector<vec3> minDist2lines(vec3 A0,vec3 B0,vec3 A1,vec3 B1);

/*! @brief This function returns 1 if the two bonds cross, 0 otherwize.
  @param a0 start of the first bond at time t
  @param b0 end of the first bond at time t
  @param a1 start of the second bond at time t
  @param b1 end of the second bond at time t
  @param A0 start of the first bond at time t+dt
  @param B0 end of the first bond at time t+dt
  @param A1 start of the second bond at time t+dt
  @param B1 end of the second bond at time t+dt
*/
int do_segments_cross(vec3 a0,vec3 b0,vec3 A0,vec3 B0,vec3 a1,vec3 b1,vec3 A1,vec3 B1);

#endif
