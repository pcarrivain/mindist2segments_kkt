CXX=g++
SRC_DIR=${PWD}
CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s -fopenmp
# CXXFLAGS=-I/usr/include -Wextra -Wall -pedantic -g -O3 -s
# CXXFLAGS=-Wextra -Wall -pedantic -g -O3 -s -mtune=native
LDFLAGS=-fopenmp
SRC_CPP=minDist2segments_KKT.cpp
OBJ=$(SRC_CPP:.cpp=.o) functions.o
EXECUTABLE=$(SRC_CPP:.cpp=)

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	$(CXX) -o $(EXECUTABLE) $(OBJ) $(LDFLAGS)

functions.o: $(SRC_DIR)/functions.cpp $(SRC_DIR)/functions.h
	$(CXX) -o functions.o -c $(SRC_DIR)/functions.cpp $(CXXFLAGS)

$(SRC_CPP:.cpp=.o): $(SRC_CPP) $(SRC_DIR)/functions.cpp $(SRC_DIR)/functions.h
	$(CXX) -o $(SRC_CPP:.cpp=.o) -c $(SRC_CPP) $(CXXFLAGS)

mrproper:
	clear
	rm -f *~
	sleep 1
	rm -f *.o
	sleep 1
	rm -f *.out
	sleep 1
	rm -f $(EXECUTABLE)
	sleep 1
