\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}vector3 Struct Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Constructor \& Destructor Documentation}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}vector3()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{5}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}vector3()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{6}{subsubsection.3.1.2.2}
\contentsline {subsection}{\numberline {3.1.3}Member Data Documentation}{6}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}x}{6}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}y}{6}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}z}{6}{subsubsection.3.1.3.3}
\contentsline {chapter}{\numberline {4}File Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}functions.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Function Documentation}{8}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}add3()}{8}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}dot\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}product()}{8}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}dot\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}product2()}{9}{subsubsection.4.1.2.3}
\contentsline {subsubsection}{\numberline {4.1.2.4}min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dist2segments\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T()}{9}{subsubsection.4.1.2.4}
\contentsline {subsubsection}{\numberline {4.1.2.5}mult3()}{10}{subsubsection.4.1.2.5}
\contentsline {subsubsection}{\numberline {4.1.2.6}normalize3()}{10}{subsubsection.4.1.2.6}
\contentsline {subsubsection}{\numberline {4.1.2.7}sub3()}{12}{subsubsection.4.1.2.7}
\contentsline {section}{\numberline {4.2}functions.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{12}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{13}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Function Documentation}{14}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}add3()}{14}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}dot\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}product()}{14}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}dot\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}product2()}{14}{subsubsection.4.2.2.3}
\contentsline {subsubsection}{\numberline {4.2.2.4}min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dist2segments\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T()}{15}{subsubsection.4.2.2.4}
\contentsline {subsubsection}{\numberline {4.2.2.5}mult3()}{16}{subsubsection.4.2.2.5}
\contentsline {subsubsection}{\numberline {4.2.2.6}normalize3()}{16}{subsubsection.4.2.2.6}
\contentsline {subsubsection}{\numberline {4.2.2.7}sub3()}{16}{subsubsection.4.2.2.7}
\contentsline {section}{\numberline {4.3}min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dist2segments\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{16}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{17}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Function Documentation}{18}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}main()}{18}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}points()}{19}{subsubsection.4.3.2.2}
\contentsline {subsection}{\numberline {4.3.3}Variable Documentation}{19}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}A0}{19}{subsubsection.4.3.3.1}
\contentsline {subsubsection}{\numberline {4.3.3.2}A1}{19}{subsubsection.4.3.3.2}
\contentsline {subsubsection}{\numberline {4.3.3.3}arg\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}i}{19}{subsubsection.4.3.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.4}B0}{19}{subsubsection.4.3.3.4}
\contentsline {subsubsection}{\numberline {4.3.3.5}B1}{19}{subsubsection.4.3.3.5}
\contentsline {subsubsection}{\numberline {4.3.3.6}enumeration}{19}{subsubsection.4.3.3.6}
\contentsline {subsubsection}{\numberline {4.3.3.7}n\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}args}{20}{subsubsection.4.3.3.7}
\contentsline {subsubsection}{\numberline {4.3.3.8}Pi}{20}{subsubsection.4.3.3.8}
\contentsline {subsubsection}{\numberline {4.3.3.9}R}{20}{subsubsection.4.3.3.9}
\contentsline {subsubsection}{\numberline {4.3.3.10}seed}{20}{subsubsection.4.3.3.10}
\contentsline {subsubsection}{\numberline {4.3.3.11}unif1}{20}{subsubsection.4.3.3.11}
\contentsline {subsubsection}{\numberline {4.3.3.12}unif2}{20}{subsubsection.4.3.3.12}
\contentsline {chapter}{Index}{21}{section*.13}
