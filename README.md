# minDist2segments_KKT

I am implementing an algorithm to find the minimal distance between two segments [A_0B_0] and [A_1B_1].
I am using the [KKT conditions](https://www2.cs.duke.edu/courses/fall19/compsci371d/notes/07_Convex%20Programming.pdf) to minimize the distance under the constraints of finite segment length.
The maxima file [minDist2segments_KKT.wxmx](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/minDist2segments_KKT.wxmx) and the pdf file [minDist2segments_KKT.pdf](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/minDist2segments_KKT.pdf) provide a detailed mathematical description.
The main functions to calculate the minimal distance are defined in the source file [functions.cpp](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/functions.cpp).
I provide an example on how to you use it as the file [minDist2segments_KKT.cpp](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/minDist2segments_KKT.cpp).
The simple option "-h" prints the basic usage of this example.
The file [refman.pdf](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/latex/refman.pdf) is built with doxygen and provide a detailed description of the functions.
The code can be compiled with the option -fopenmp to activate the pragma omp parallel sections.
However, it seems to be slower.
I will prodive soon a benchmark.

# Implementation for **LAMMPS**

I prodive some [modifications](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/for_SRP/) to the [SRP fix for LAMMPS](http://lammps.sandia.gov/doc/pair_srp.html) to include the minimal distance between two segments with KKT conditions.
Indeed, SRP fix computes the minimal distance between two infinite lines and reset the solution to occur along the interior of the bond.
This method is not always accurate.
KKT conditions allows to compute the minimal distance such finite length segment constraint holds.
I used the **lammps-7Aug19** version with the following command lines.
```bash
cd lammps-7Aug19/src;
make clean-all;
make yes-molecule;
make yes-opt;
make yes-user-omp;
make yes-user-misc;
make yes-user-dpd;
make serial;
```
You need to copy the [header pair_srp.h](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/for_SRP/pair_srp.h) and [source pair_srp.cpp](https://gitlab.com/pcarrivain/mindist2segments_kkt/-/blob/master/for_SRP/pair_srp.cpp) files to the **lammps-7Aug19/src/USER-MISC/** folder.
In order to use the modifications you need to pass *min_KKT* to the distance option (see SRP description).
For more details you can have a look to the [LAMMPS SRP fix input file example](https://github.com/lammps/lammps/blob/master/examples/USER/misc/srp/in.srp) and [data file](https://github.com/lammps/lammps/blob/master/examples/USER/misc/srp/data.chain) I used to run simulations (I changed *mid* to *min_KKT* as-well-as SRP parameters).
To test the function *getMinDist_KKT* I wrote an *exact enumeration* of all the possible distances between two segments.
The *exact enumeration* cuts each segment into 40000 points and runs a nested loop over all the possible pairwize.
If you pass *min_KKT_test* to the distance option the code will print the results from the three differents methods.