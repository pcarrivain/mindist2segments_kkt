/*!
  \file minDist2segments_KKT.cpp is distributed under MIT licence.
  \brief Main code used in the calculation of the minimal distance between segment [A0,B0=A0+L0*T0] and segment [A1,B1=A1+L1*T1] using the Karush-Kuhn-Tucker conditions.
  \author Pascal Carrivain
  \date Monday 9 March 2020

  Minimize d(r,s)^2=(A0+r*T0-(A1+s*T1))^2=(DELTA+r*T0-s*T1)^2 under the conditions :

  r>=0.

  s>=0.

  L0-r>=0.

  L1-s>=0.

  dot(T,T)=1.

  The parameters you need to pass are the coordinates A0x A0y A0z B0x B0y B0z A1x A1y A1z B1x B1y B1z of the
  points A0, B0=A0+L0*T0, A1 and B1=A1+L1*T1. Typical use is simply :

  "./executable_name --A0x A0x --A0y A0y --A0z A0z --B0x B0x --B0y B0y --B0z B0z --A1x A1x --A1y A1y --A1z A1z --B1x B1x --B1y B1y --B1z B1z".

  If you do not pass arguments through the command line, the program generates two random segments. At the end, it compares
  the KKT results with an enumeration of all the pairwise between the two segments. It is a way to test if our method agrees
  with "exact" enumeration result. Typical use is simply given by :

  "./executable_name -h"
*/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <random>
#include <vector>
#include <getopt.h>
#include "functions.h"

using namespace std;

vec3 a0;/*!< start of the first segment */
vec3 b0;/*!< end of the first segment */
vec3 a1;/*!< start of the second segment */
vec3 b1;/*!< end of the second segment */
vec3 A0;/*!< start of the first segment */
vec3 B0;/*!< end of the first segment */
vec3 A1;/*!< start of the second segment */
vec3 B1;/*!< end of the second segment */
vector<vec3> points(2);/*!< to store the two points associated with the minimal distance */
double unif1;/*!< uniform random number */
double unif2;/*!< uniform random number */
double R=1.;/*!< default size of the space containing the segments (unit sphere) */
const double Pi=acos(-1.);/*!< Pi constant */
int n_args=0;/*!< the number of coordinates passed through the command line */
int arg_i;/*!< variable for the getopt switch */
int seed=0;/*!< seed for the pRNGs */
int enumeration=10000;/*!< the enumeration length */
char mode[100];

int main(int argc,char **argv){
  /*! getopt definition to get the arguments through the command line
   */
  const option long_opts[]={
    {"a0x",required_argument,nullptr,'a'},
    {"a0y",required_argument,nullptr,'b'},
    {"a0z",required_argument,nullptr,'c'},
    {"b0x",required_argument,nullptr,'d'},
    {"b0y",required_argument,nullptr,'e'},
    {"b0z",required_argument,nullptr,'f'},
    {"a1x",required_argument,nullptr,'A'},
    {"a1y",required_argument,nullptr,'B'},
    {"a1z",required_argument,nullptr,'C'},
    {"b1x",required_argument,nullptr,'D'},
    {"b1y",required_argument,nullptr,'E'},
    {"b1z",required_argument,nullptr,'F'},
    {"seed",required_argument,nullptr,'s'},
    {"enumeration",required_argument,nullptr,'p'},
    {"mode",required_argument,nullptr,'m'},
    {"help",optional_argument,nullptr,'h'}
  };
  /*! Get the two segments from command line arguments ...
   */
  while((arg_i=getopt_long(argc,argv,"a:b:c:d:e:f:A:B:C:D:E:F:s:p:m:h",long_opts,nullptr))!=-1){
    switch(arg_i){
    case 'a':
      a0.x=atof(optarg);
      n_args++;
      break;
    case 'b':
      a0.y=atof(optarg);
      n_args++;
      break;
    case 'c':
      a0.z=atof(optarg);
      n_args++;
      break;
    case 'd':
      b0.x=atof(optarg);
      n_args++;
      break;
    case 'e':
      b0.y=atof(optarg);
      n_args++;
      break;
    case 'f':
      b0.z=atof(optarg);
      n_args++;
      break;
    case 'A':
      a1.x=atof(optarg);
      n_args++;
      break;
    case 'B':
      a1.y=atof(optarg);
      n_args++;
      break;
    case 'C':
      a1.z=atof(optarg);
      n_args++;
      break;
    case 'D':
      b1.x=atof(optarg);
      n_args++;
      break;
    case 'E':
      b1.y=atof(optarg);
      n_args++;
      break;
    case 'F':
      b1.z=atof(optarg);
      n_args++;
      break;
    case 's':
      seed=(int)atof(optarg);
      break;
    case 'p':
      enumeration=(int)atof(optarg);
      break;
    case 'm':
      printf(mode,"%s",optarg);
      break;
    case 'h':
      printf("\nMinimal distance between two segments (using the Karush-Kuhn-Tucker conditions), usage :\n");
      printf("\n");
      printf("      Compute the minimal distance between two segments [a0,b0] and [a1,b1] :\n");
      printf("      ./executable_name --a0x a0x --a0y a0y --a0z a0z --b0x b0x --b0y b0y --b0z b0z --a1x a1x --a1y a1y --a1z a1z --b1x b1x --b1y b1y --b1z b1z\n");
      printf("      a0x : x coordinate of the first point of the first segment.\n");
      printf("      a0y : y coordinate of the first point of the first segment.\n");
      printf("      a0z : z coordinate of the first point of the first segment.\n");
      printf("      b0x : x coordinate of the second point of the first segment.\n");
      printf("      ...\n");
      printf("      a1x : x coordinate of the first point of the second segment.\n");
      printf("      ...\n");
      printf("      b1x : x coordinate of the second point of the second segment.\n");
      printf("      ...\n");
      printf("\n");
      printf("      Compute minimal distance between two random segments (with seed 'seed') and computes the minimal distance between them :\n");
      printf("      ./executable_name -s (or --seed) seed -p (or --enumeration) enumeration\n");
      printf("      ./executable_name -s (or --seed) 1 -p (or --enumeration) 10000\n");
      printf("      The result is compared to an enumeration method of length 10000 (each segment is splitted in 10000 points) :\n");
      printf("      The code runs a loop over all the possible pairwize (p0i,p1j) and keeps the minimal distance.\n");
      printf("      Then, the code prints the minimal distance computed with the enumeration and KKT method as-well-as the coordinates of the two segments.\n");
      printf("      If the enumeration method and KKT conditions give different result the code prints a warning.\n");
      printf("      The warning can be removed by increasing the number of points used by the enumeration method.\n");
      printf("      If not, please consider sending me a mail p.carrivain_at_gmail.com.\n");
      return 0;
      break;
    }
  }
  if(n_args!=12){
    /*! If no arguments have been passed through command line, the code generates two random segments ...
     */
    /*! pRNGs initialisation
     */
    std::default_random_engine e1(seed);
    std::uniform_real_distribution<double> uniform_real_dist(0.0,1.0);
    /*! First segment :
      Start of the first segment, uniformly distributed inside a sphere of radius R.
     */
    unif1=2.*Pi*uniform_real_dist(e1);
    unif2=acos(fmin(1.,fmax(-1.,2.*uniform_real_dist(e1)-1.)));
    a0=mult3(R*uniform_real_dist(e1),vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2)));
    /*! End of the first segment, uniformly distributed inside a sphere of radius R.
     */
    unif1=2.*Pi*uniform_real_dist(e1);
    unif2=acos(fmin(1.,fmax(-1.,2.*uniform_real_dist(e1)-1.)));
    b0=mult3(R*uniform_real_dist(e1),vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2)));
    /*! Second segment :
      Start of the first segment, uniformly distributed inside a sphere of radius R.
     */
    unif1=2.*Pi*uniform_real_dist(e1);
    unif2=acos(fmin(1.,fmax(-1.,2.*uniform_real_dist(e1)-1.)));
    a1=mult3(R*uniform_real_dist(e1),vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2)));
    /*! End of the second segment, uniformly distributed inside a sphere of radius R.
     */
    unif1=2.*Pi*uniform_real_dist(e1);
    unif2=acos(fmin(1.,fmax(-1.,2.*uniform_real_dist(e1)-1.)));
    b1=mult3(R*uniform_real_dist(e1),vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2)));
  }
  /*! Calculate the minimal distance and test the result.
   */
  // points=minDist2segments_KKT(a0,b0,a1,b1,true,enumeration);
  /*! Check the function to detect crossing.
   */
  vec3 shift=sub3(mult3(.5,add3(a0,b0)),mult3(.5,add3(a1,b1)));
  int cross=0;
  cross=do_segments_cross(a0,b0,a0,b0,add3(a1,mult3(1.-1./enumeration,shift)),add3(b1,mult3(1.-1./enumeration,shift)),add3(a1,mult3(1.+1./enumeration,shift)),add3(b1,mult3(1.+1./enumeration,shift)));
  if(cross!=0){
    printf("--- cross=%i\n",cross);
    return 0;
  }
  return 0;
}
