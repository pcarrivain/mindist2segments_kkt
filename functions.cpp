/*!
  \file functions.cpp distributed under MIT licence.
  \brief Source file for functions used in the calculation of minimal distance between segment [A0,B0=A0+L0*T0] and segment [A1,B1=A1+L1*T1].
  \author Pascal Carrivain
  \date Monday 09 March 2020
*/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <assert.h>
#include <vector>
#include <cfloat>
#include "functions.h"

using namespace std;

/*! return the dot-product of u with v.
 */
double dot_prod(vec3 u,vec3 v){
  return u.x*v.x+u.y*v.y+u.z*v.z;
}

/*! return the dot-product of u with itself.
 */
double dot_prod2(vec3 u){
  return u.x*u.x+u.y*u.y+u.z*u.z;
}

/*! return a*u where a is a scalar.
 */
vec3 mult3(double a,vec3 u){
  u.x*=a;
  u.y*=a;
  u.z*=a;
  return u;
}

/*! return u+v where u and v are vec3.
 */
vec3 add3(vec3 u,vec3 v){
  u.x+=v.x;
  u.y+=v.y;
  u.z+=v.z;
  return u;
}

/*! return u-v where u and v are vec3.
 */
vec3 sub3(vec3 u,vec3 v){
  u.x-=v.x;
  u.y-=v.y;
  u.z-=v.z;
  return u;
}

/*! return a vec3 u normalized to 1.
  if the norm is 0 return u.
 */
vec3 normalize3(vec3 u){
  double n=sqrt(u.x*u.x+u.y*u.y+u.z*u.z);
  if(n>0.){
    u.x/=n;
    u.y/=n;
    u.z/=n;
  }
  return u;
}

/*! @brief This function returns the two points (each on one segment) such that the distance is minimal.
  This function compares the result to an "exact enumeration" too (if test_result is true).
  The number of points (along each segment) is given by the integer variable enumeration.
  The precision of the "enumeration" increases with the integer variable enumeration.
  The function uses OpenMP "section" functionality to handle the different cases.
  @param A0 start of the first bond
  @param B0 end of the first bond
  @param A1 start of the second bond
  @param B1 end of the second bond
  @param test_result does the function test the result with an "exact enumeration" ?
  @param enumeration number of points to consider in the "exact enumeration"
*/
vector<vec3> minDist2segments_KKT(vec3 A0,vec3 B0,vec3 A1,vec3 B1,bool test_result,int enumeration){
  double r_star[9];
  double s_star[9];
  double i_star=-1.;
  double j_star=-1.;
  double r_star_min=-1.;
  double s_star_min=-1.;
  /*! Segment length of each of the two segments
   */
  double l0=sqrt(dot_prod2(sub3(B0,A0)));
  double l1=sqrt(dot_prod2(sub3(B1,A1)));
  /*! Tangent vectors of each of the two segments
   */
  vec3 T0=normalize3(sub3(B0,A0));
  vec3 T1=normalize3(sub3(B1,A1));
  /*! Dot product between tangent segments
   */
  double cos01=dot_prod(T0,T1);
  /* Distance vector between origin point of the two segments.
   */
  vec3 DELTA=sub3(A0,A1);
  double T0_DELTA=dot_prod(T0,DELTA);
  double T1_DELTA=dot_prod(T1,DELTA);
  #pragma omp parallel sections
  {
    #pragma omp section
    {
      /*! Solutions for r and s inside the feasible region.
       */
      /*! solution 1 is not valid if the two segments are parallel.
       */
      if(cos01>-.99999 && cos01<.99999){
	r_star[0]=(cos01*T1_DELTA-T0_DELTA)/(1.-cos01*cos01);
	s_star[0]=(T1_DELTA-cos01*T0_DELTA)/(1.-cos01*cos01);
      }else{
	r_star[0]=-1.;
	s_star[0]=-1.;
      }
      /*! Solutions for r and s outside the feasible region.
       */
      /*! solution 2
       */
      r_star[1]=0.;
      s_star[1]=T1_DELTA;
      /*! solution 3
       */
      r_star[2]=-T0_DELTA;
      s_star[2]=0.;
      /*! solution 4
       */
      r_star[3]=0.;
      s_star[3]=0.;
    }
    #pragma omp section
    {
      /*! solution 5
       */
      r_star[4]=l0;
      s_star[4]=l0*cos01+T1_DELTA;
      /*! solution 6
       */
      r_star[5]=l0;
      s_star[5]=0.;
      /*! solution 7
       */
      r_star[6]=l1*cos01-T0_DELTA;
      s_star[6]=l1;
      /*! solution 8
       */
      r_star[7]=0.;
      s_star[7]=l1;
      /*! solution 9
       */
      r_star[8]=l0;
      s_star[8]=l1;
    }
  }
  /*! min of d^2, loop over the nine previous solutions
   */
  double d2_min_KKT=DBL_MAX,d2;
  for(int i=0;i<9;i++){
    /*! The solution is valid if and only if the two points are lying to the segments and the corresponding distance is minimal.
     */
    if(!(r_star[i]<0. || r_star[i]>l0 || s_star[i]<0. || s_star[i]>l1)){
      d2=dot_prod2(sub3(add3(A0,mult3(r_star[i],T0)),add3(A1,mult3(s_star[i],T1))));
      /*! Keep the minimal distance amongst the nine solutions.
       */
      if(d2<d2_min_KKT){
	d2_min_KKT=d2;
	r_star_min=r_star[i];
	s_star_min=s_star[i];
      }
    }
  }
  /*! Enumerate all the possible distances (segment 0,segment 1) and keep the star one
   */
  double abs_tol=1e-5;
  double rel_tol=1e-3;
  double inv_enumeration=1./enumeration;
  double d2_min_enum=DBL_MAX;
  double tmp_j_star;
  vec3 P_STAR;
  if(test_result){
    // loop over the first segment discrete parts
    for(int i=0;i<=enumeration;i++){
      P_STAR=add3(A0,mult3(i*l0*inv_enumeration,T0));
      // vector from A1 to P_STAR projected along T1
      tmp_j_star=fmin(l1,fmax(.0,dot_prod(sub3(P_STAR,A1),T1)));
      // square distance
      d2=dot_prod2(sub3(P_STAR,add3(A1,mult3(tmp_j_star,T1))));
      // temporary minimal distance ?
      if(d2<d2_min_enum){
	d2_min_enum=d2;
	i_star=i*inv_enumeration*l0;
	j_star=tmp_j_star;
      }
    }
    printf("The minimal distance between segment 1 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n",A0.x,A0.y,A0.z,B0.x,B0.y,B0.z);
    printf("and segment 2 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n",A1.x,A1.y,A1.z,B1.x,B1.y,B1.z);
    printf("is :\n");
    printf("KKT conditions : position along segment 1 is %f, position along segment 2 is %f, minimal distance is %f\n",r_star_min/l0,s_star_min/l1,sqrt(d2_min_KKT));
    printf("enumeration : position along segment 1 is %f, position along segment 2 is %f, minimal distance is %f\n",i_star/l0,j_star/l1,sqrt(d2_min_enum));
    /*! Check for differences between KKT conditions and enumeration results.
     */
    if(fabs(i_star-r_star_min)>(abs_tol+rel_tol*r_star_min) || fabs(j_star-s_star_min)>(abs_tol+rel_tol*s_star_min) || fabs(sqrt(d2_min_enum)-sqrt(d2_min_KKT))>(abs_tol+rel_tol*sqrt(d2_min_KKT))){
      for(int i=0;i<1000000;i++){
	printf("warning : the difference between KKT and enumeration solutions does not satisfy ||x-y||<%.5f+%.3f*y.\n",abs_tol,rel_tol);
	printf("          fabs(min_enum-min_KKT)=%e and %.5f+%.3f*min_KKT=%e\n",fabs(sqrt(d2_min_enum)-sqrt(d2_min_KKT)),abs_tol,rel_tol,abs_tol+rel_tol*sqrt(d2_min_KKT));
	printf("          fabs(p0_enum-p0_KKT)=%e and %.5f+%.3f*p0_KKT=%e\n",fabs(i_star-r_star_min),abs_tol,rel_tol,abs_tol+rel_tol*r_star_min);
	printf("          fabs(p1_enum-p1_KKT)=%e and %.5f+%.3f*p1_KKT=%e\n\n",fabs(j_star-s_star_min),abs_tol,rel_tol,abs_tol+rel_tol*s_star_min);
      }
    }else printf("\n");
  }
  /*! Return the result.
   */
  vector<vec3> points(2);
  points[0]=add3(A0,mult3(r_star_min,T0));
  points[1]=add3(A1,mult3(s_star_min,T1));
  return points;
}

/*! @brief This function returns the two points (each on one line) such that the distance is minimal.
  @param A0 start of the first bond
  @param B0 end of the first bond
  @param A1 start of the second bond
  @param B1 end of the second bond
*/
vector<vec3> minDist2lines(vec3 A0,vec3 B0,vec3 A1,vec3 B1){
  double r_star,s_star;
  /*! Segment length of each of the two lines
   */
  double l0=sqrt(dot_prod2(sub3(B0,A0)));
  double l1=sqrt(dot_prod2(sub3(B1,A1)));
  /*! Tangent vectors of each of the two lines
   */
  vec3 T0=normalize3(sub3(B0,A0));
  vec3 T1=normalize3(sub3(B1,A1));
  /*! Dot product between tangent segments
   */
  double cos01=dot_prod(T0,T1);
  /* Distance vector between origin point of the two lines.
   */
  vec3 DELTA=sub3(A0,A1);
  double T0_DELTA=dot_prod(T0,DELTA);
  double T1_DELTA=dot_prod(T1,DELTA);
  /*! solution is not valid if the two segments are parallel.
   */
  if(cos01>-.99999 && cos01<.99999){
    r_star=(cos01*T1_DELTA-T0_DELTA)/(1.-cos01*cos01);
    s_star=(T1_DELTA-cos01*T0_DELTA)/(1.-cos01*cos01);
  }else{
    r_star=-1.;
    s_star=-1.;
  }
  /*! Return the result.
   */
  vector<vec3> points(2);
  points[0]=add3(A0,mult3(r_star,T0));
  points[1]=add3(A1,mult3(s_star,T1));
  return points;
}

/*! @brief This function returns 1 if the two bonds cross, -1 if the distance at time t is 0, 0 otherwize.
  @param a0 start of the first bond at time t
  @param b0 end of the first bond at time t
  @param a1 start of the second bond at time t
  @param b1 end of the second bond at time t
  @param A0 start of the first bond at time t+dt
  @param B0 end of the first bond at time t+dt
  @param A1 start of the second bond at time t+dt
  @param B1 end of the second bond at time t+dt
*/
int do_segments_cross(vec3 a0,vec3 b0,vec3 A0,vec3 B0,vec3 a1,vec3 b1,vec3 A1,vec3 B1){
  vector<vec3> points(2),POINTS(2);
  // tolerance
  double abs_tol=.000000001;
  // tangents
  vec3 T0=sub3(b0,a0);
  vec3 T1=sub3(b1,a1);
  // segments lengths
  double l0=sqrt(dot_prod2(T0));
  double l1=sqrt(dot_prod2(T1));
  // minimal distance vector (between the two segments) at time t
  points=minDist2segments_KKT(a0,b0,a1,b1,false,10000);
  vec3 distance_vec3=sub3(points[0],points[1]);
  double d2=dot_prod2(distance_vec3);
  // if the distance at time t is zero, return -1
  if(sqrt(d2)<abs_tol) return -1;
  // minimal distance vector (between the two lines) at time t
  points=minDist2lines(a0,b0,a1,b1);
  // minimal distance vector (between the two lines) at time t+dt
  POINTS=minDist2lines(A0,B0,A1,B1);
  // square distances
  distance_vec3=sub3(points[0],points[1]);
  d2=dot_prod2(distance_vec3);
  double D2=dot_prod2(sub3(POINTS[0],POINTS[1]));
  // delta r and delta s
  double r0=sqrt(dot_prod2(sub3(points[0],a0)));
  double s0=sqrt(dot_prod2(sub3(points[1],a1)));
  double r1=sqrt(dot_prod2(sub3(POINTS[0],A0)));
  double s1=sqrt(dot_prod2(sub3(POINTS[1],A1)));	
  double dr=r1-r0;
  double ds=s1-s0;
  // x
  double xx=-.5*d2/dot_prod(distance_vec3,sub3(mult3(dr,T0),mult3(ds,T1)));
  printf("dr=%e ds=%e d2=%e D2=%e : %e\n",dr,ds,d2,D2,xx);//dot_prod(distance_vec3,sub3(mult3(dr,T0),mult3(ds,T1))));//sqrt(dot_prod2(sub3(mult3(dr,T0),mult3(ds,T1)))));
  // bond lengths at xx ?
  return (int)(xx>.0 && xx<1. && (r0+xx*dr)>=.0 && (r0+xx*dr)<=l0 && (s0+xx*ds)>=.0 && (s0+xx*ds)<=l1);
}
